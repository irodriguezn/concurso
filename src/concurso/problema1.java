/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package concurso;

import java.util.Scanner;

public class problema1 {
    public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            int numCasos=sc.nextInt();
            int numMascletas;
            int max, min, siguiente;
            for (int i=1; i<=numCasos; i++) {
                numMascletas=sc.nextInt();
                max=0;
                min=100000;
                for (int j=0; j<numMascletas; j++) {
                    siguiente=sc.nextInt();
                    if (siguiente>max) {
                        max=siguiente;
                    }
                    if (siguiente<min) {
                        min=siguiente;
                    }
                } 
                System.out.println(max + " " + min);
            }
    }
}

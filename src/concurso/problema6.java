/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package concurso;

import java.util.Scanner;

public class problema6 {
    public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            int numEquipos;
            long resultado;
            int numCasos=sc.nextInt();
            
            for (int i=1; i<=numCasos; i++) {
                numEquipos=sc.nextInt();
                resultado=combinaciones(numEquipos*3,2);
                System.out.println(resultado);
            }
    }

    static long factorial(long n) {
        if (n==0) {
            return 1;
        } else {
            return n * factorial(n-1);
        }
    }
    
    static long combinaciones(long n, long k) {
        if (n<=0) {
            return 0;
        } else {
            return (factorial(n)/(factorial(k)*factorial(n-k)));
        }
    }
}

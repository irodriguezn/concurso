/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package concurso;

import java.util.ArrayList;
import java.util.Scanner;

public class problema3 {
    public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            int numProblemas, salto;
            String respuesta;
            ArrayList<Integer> problemas=new ArrayList<Integer>();
            int numCasos=sc.nextInt();
            for (int i=1; i<=numCasos; i++) {
                numProblemas=sc.nextInt();
                salto=sc.nextInt();
                for (int j=2; j<=numProblemas; j++) {
                    problemas.add(j);
                }
                respuesta=calculaSalto(problemas, salto);
                System.out.println(respuesta);
            }
    }
    
    static String calculaSalto(ArrayList<Integer> problemas, int salto) {
        String resultado="1";
        int posActual=0;
        int pos, tamaño=problemas.size();
        while (! problemas.isEmpty()) {
            pos=posActual+salto-1;
            while (pos>tamaño-1 && tamaño!=0) {
                pos=pos-tamaño;
            }
            resultado= problemas.get(pos) + "";
            problemas.remove(pos);
            tamaño=problemas.size();
            while (pos>tamaño-1 && tamaño!=0) {
                pos=pos-tamaño;
            }            
            posActual=pos;
        }
        return resultado;
    }
}

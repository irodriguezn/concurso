/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package concurso;

import java.util.Scanner;

public class problema2 {
    public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            String palabra;
            boolean tieneMay, tieneMin, tieneDig, tieneEsp, correcta;
            int numCasos=Integer.parseInt(sc.next());
            String minusculas="abcdefghijklmnñopqrstuvwxyz";
            String mayusculas="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            String especiales="+_)(*&^%$#@!./,;{}";
            String digitos="0123456789";
            String caracter;
            for (int i=1; i<=numCasos; i++) {
                //resolver el problema
                tieneMay=false;
                tieneMin=false;
                tieneDig=false;
                tieneEsp=false;
                correcta=false;
                palabra=sc.next();
                if (palabra.length()>=12) {
                    for (int j=0; j<palabra.length(); j++) {
                        caracter=palabra.charAt(j) + "";
                        if (mayusculas.indexOf(caracter)!=-1) {
                            tieneMay=true;
                        } else if (minusculas.indexOf(caracter)!=-1) {
                            tieneMin=true;
                        } else if (especiales.indexOf(caracter)!=-1) {
                            tieneEsp=true;
                        } else if (digitos.indexOf(caracter)!=-1) {
                            tieneDig=true;
                        } 
                        
                        if (tieneMay && tieneMin && tieneDig && tieneEsp) {
                            correcta=true;
                            break;
                        }                        
                    }
                    if (correcta) {
                        System.out.println("OK");
                    } else {
                        System.out.println("ERROR");
                    }
                } else {
                    System.out.println("ERROR");
                }
            }
    }
}
